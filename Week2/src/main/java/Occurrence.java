public class Occurrence {

    /**
     * Takes as parameters:
     * @param arr and
     * @param v and
     * @param c and
     * @return the number of times the given number occurs
     */
    static int countOccurrence(int arr[], int v, int c)
    {
        int counter = 0;
        for (int i=0; i<v; i++)
            if (c == arr[i])
                counter++;
        return counter;
    }
}
