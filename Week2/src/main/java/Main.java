public class Main {


    public static void main(String[] args) {

        int[] A = {8, 7, 2, 5, 3, 1};
        int sum = 10;
/**
 * Calls the method findPair
 */
        IntegerFind.findPair(A, sum);
        int[] myArray = {5, 2, 3, 5, 7, 9, 7, 1, 0};
/**
 * Calls the method linearIndexSearch and finds the index of 3
 */
        System.out.println("The Index of 3 is: " + LinearIndexSearch.linearIndexSearch(myArray, 3));
        /**
         * Calls the method linearIndexSearch and finds the index of 9
         */
        System.out.println("The Index of 9 is: " + LinearIndexSearch.linearIndexSearch(myArray, 9));



        int[] myArray2 = {0, 2, 3, 5, 7, 8, 9,};
        /**
         * Calls the method binaryIndexSearch and finds the index of 3
         */
        System.out.println("The Index of 3 is: " + BinaryIndexSearch.binaryIndexSearch(myArray2, 3));
        /**
         *  Calls the method linearIndexSearch and finds the index of 9
         */
        System.out.println("The Index of 9 is: " + BinaryIndexSearch.binaryIndexSearch(myArray2, 9));


        /**
         * Instantiates an array with duplicates and finds the occurrences of a given number
         */
        int myArray3[] = {1,5,5,6,6,6,6,8,8,8,9};
        int n = myArray3.length;
        int x = 6;
        System.out.println("Element 6 occurs: " +Occurrence.countOccurrence(myArray3, n, x)+" " +"times");
    }
}

