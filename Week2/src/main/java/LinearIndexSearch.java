public class LinearIndexSearch {

    /**
     * Takes as input an array :
     * @param Ar, and a number :
     * @param x and
     * @return the index position of that number in the array
     */
    public static int linearIndexSearch(int Ar[], int x) {

        /**
         * If the array is null
         */
        if (Ar == null) {
            return -1;
                        }
        int Arlen = Ar.length;
        int i = 0;

        /**
         * Search in the array
         */
        while (i < Arlen) {
            if (Ar[i] == x) {
                return i;
            }
            else {
                i = i + 1;
                 }
          }
        return -1;
    }
}
