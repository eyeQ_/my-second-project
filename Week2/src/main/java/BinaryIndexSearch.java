import java.util.Arrays;

public class BinaryIndexSearch {

    /**
     * Takes as input an array :
     * @param arr, and a number:
     * @param o, and
     * @return  the index position of that number in the array
     */
    public static int binaryIndexSearch(int arr[], int o)
    {

        int index = Arrays.binarySearch(arr, o);
        return (index < 0) ? -1 : index;
    }


}
