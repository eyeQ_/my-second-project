

public class IntegerFind {

    /**
     * Takes as input an  unsorted array of integers:
     * @param a , and a given sum :
     * @param sum and finds the pair
     */
     public static void findPair(int[] a, int sum)
        {
        for (int i = 0; i < a.length - 1; i++)
                   {
            for (int h = i + 1; h < a.length; h++)
            {
                if (a[i] + a[h] == sum)
                {
                    System.out.println("Pair exists at index "
                            + i + " and " + h);
                    return;
                }
            }
                  }
        System.out.println("Pair doesn't exist ");
         }


}
