package Services;

public class Tree {


    Node root1, root2;

    /**
     *  takes as input two nodes:
     * @param c and
     * @param d and
     * @return if they are identical or empty
     */
    public boolean identicalTrees(Node c, Node d)
    {
        // If both empty
        if (c == null && d == null)
            return true;

        //If both not empty then compares them
        if (c != null && d != null)
            return (c.contend == d.contend
                    && identicalTrees(c.left, d.left)
                    && identicalTrees(c.right, d.right));

        // If one is empty and one isn't
        return false;
    }



}
