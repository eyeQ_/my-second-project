package Services;


public class TestTree {

    public static void main(String[] args)

    /**
     * Create two trees and decides if they are identical or not
     */
    {
        Tree tree = new Tree();

        tree.root1 = new Node(4);
        tree.root1.left = new Node(5);
        tree.root1.right = new Node(6);
        tree.root1.left.left = new Node(7);
        tree.root1.left.right = new Node(8);

        tree.root2 = new Node(4);
        tree.root2.left = new Node(5);
        tree.root2.right = new Node(6);
        tree.root2.left.left = new Node(7);
        tree.root2.left.right = new Node(8);

        if (tree.identicalTrees(tree.root1, tree.root2))
            System.out.println("Both trees are identical");
        else
            System.out.println("Trees are NOT identical");

    }

}
