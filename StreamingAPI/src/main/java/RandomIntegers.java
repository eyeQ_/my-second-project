
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;



public class RandomIntegers {

    public static void main(String args[]) {
/**
 * Create randomNumbers List with 1000 random integers
 * Create oddNumbers list with only the odd numbers of the randomNumbers list
 * Create evenNumbers list with only the odd numbers of the evenNumbers list
 */
        Random random = new Random();
        List<Integer> randomNumbers = random.ints(1000).sorted().boxed().collect(Collectors.toList());
        List<Integer> oddNumbers = randomNumbers.stream().filter(i -> i % 2 != 0).collect(Collectors.toList());
        List<Integer> evenNumbers = randomNumbers.stream().filter(i-> i % 2 ==0).collect(Collectors.toList());
/**
 * Find the max number,the min number, the sum and the average from a list of 1000 random numbers
 */
        int min = randomNumbers.stream().mapToInt(i->i).min().getAsInt();
        int max = randomNumbers.stream().mapToInt(i->i).max().getAsInt();
        double sum = randomNumbers.stream().mapToInt(i -> i).sum();
        double avg = randomNumbers.stream().mapToInt(i -> i).average().getAsDouble();

        System.out.println(min);
        System.out.println(max);
        System.out.println(sum);
        System.out.println(avg);

        /**
         * Take a list of Strings and create another list filtering out all empty strings
         */
        List<String> originalList = Arrays.asList("Java", "avh", "", "Trw", "y59", "ODa", "", "226");
        System.out.println(originalList);
        List<String> notEmptyList = originalList.stream().filter(String->!String.isEmpty()).collect(Collectors.toList());
        System.out.println(notEmptyList);

    }
}
