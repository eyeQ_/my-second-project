public class BinaryTree {

        private Node root;

        public BinaryTree()
        {
            root = null;
        }

    /**
     * checks if the tree is empty and
     * @return null
     */
    public boolean isEmpty()
        {
            return root == null;
        }

    /**
     * Method that takes :
     * @param data and inserts them
     */
    public void insert(int data)
        {
            root = insert(root, data);
        }

    /**
     * Method that takes as parameters :
     * @param node and
     * @param data and
     * @return recursively the nodes
     */
        private Node insert(Node node, int data)
        {
            if (node == null)
                node = new Node(data);
            else
            {
                if (data <= node.getData())
                    node.left = insert(node.left, data);
                else
                    node.right = insert(node.right, data);
            }
            return node;
        }

    /**
     * Method that deletes the specific
     * @param k
     */
    public void delete(int k)
        {
            if (isEmpty())
                System.out.println("Empty");
            else if (search(k) == false)
                System.out.println(k +"does not exists");
            else
            {
                root = delete(root, k);
                System.out.println(k+ " deleted ");
            }
        }

    /**
     * Takes the root
     * @param root and the data
     * @param d of the node and
     * @return deletes it
     */
        private Node delete(Node root, int d)
        {
            Node x1, x2, n;
            if (root.getData() == d)
            {
                Node rleft, rright;
                rleft = root.getLeft();
                rright = root.getRight();
                if (rleft == null && rright == null)
                    return null;
                else if (rleft == null)
                {
                    x1 = rright;
                    return x1;
                }
                else if (rright == null)
                {
                    x1 = rleft;
                    return x1;
                }
                else
                {
                    x2 = rright;
                    x1 = rright;
                    while (x1.getLeft() != null)
                        x1 = x1.getLeft();
                    x1.setLeft(rleft);
                    return x2;
                }
            }
            if (d < root.getData())
            {
                n = delete(root.getLeft(), d);
                root.setLeft(n);
            }
            else
            {
                n = delete(root.getRight(), d);
                root.setRight(n);
            }
            return root;
        }

    /**
     * Counts the numbers of nodes and
     * @return the numbers of nodes
     */
    public int countNodes()
        {
            return countNodes(root);
        }

    /**
     * counts the numbers of nodes
     * @param nd and
     * @return them recursively
     */
    private int countNodes(Node nd)
        {
            if (nd == null)
                return 0;
            else
            {
                int l = 1;
                l += countNodes(nd.getLeft());
                l += countNodes(nd.getRight());
                return l;
            }
        }

    /**
     * Method that takes the
     * @param value, searches for it
     * @return returns it
     */
    public boolean search(int value)
        {
            return search(root, value);
        }

    /**
     * method that takes as parameters the node
     * @param nd  and the value
     * @param value  to search,searches for it recursively and returns
     * @return the results of the search
     */
        private boolean search(Node nd, int value)
        {
            boolean found = false;
            while ((nd != null) && !found)
            {
                int rval = nd.getData();
                if (value < rval)
                    nd = nd.getLeft();
                else if (value > rval)
                    nd = nd.getRight();
                else
                {
                    found = true;
                    break;
                }
                found = search(nd, value);
            }
            return found;
        }


        public int findMax()
        {
            return findMax(root);
        }
        // counts the number of nodes recursively
        private int findMax(Node r)
        {
            if (r == null)
                return Integer.MIN_VALUE;
            else
            {
                int max = r.getData();
                int lmax = findMax(r.getLeft());
                int rmax = findMax(r.getRight());

                if (lmax > rmax)
                    rmax = lmax;
                if (rmax > max)
                    max = rmax;
                return max;
            }
        }
        public int findMin()
        {
            return findMin(root);
        }
        // Counts the number of nodes recursively
        private int findMin(Node nd)
        {
            if (nd == null)
                return Integer.MAX_VALUE;
            else
            {
                int min = nd.getData();
                int lmin = findMin(nd.getLeft());
                int rmin = findMin(nd.getRight());

                if (lmin < min)
                    min = lmin;
                if (rmin < min)
                    min = rmin;
                return min;
            }
        }


        // In-order traversal
        public void inorder()
        {
            inorder(root);
        }
        private void inorder(Node nd)
        {
            if (nd != null)
            {
                inorder(nd.getLeft());
                System.out.print(nd.getData() +" ");
                inorder(nd.getRight());
            }
        }
        // Pre-order traversal
        public void preorder()
        {
            preorder(root);
        }
        private void preorder(Node nd)
        {
            if (nd != null)
            {
                System.out.print(nd.getData() +" ");
                preorder(nd.getLeft());
                preorder(nd.getRight());
            }
        }
        // Post-order traversal
        public void postorder()
        {
            postorder(root);
        }
        private void postorder(Node nd)
        {
            if (nd != null)
            {
                postorder(nd.getLeft());
                postorder(nd.getRight());
                System.out.print(nd.getData() +" ");
            }
        }
    }


