public class Node {

    Node left, right;
    int data;

    public Node() {
        left = null;
        right = null;
        data = 0;
    }

    public Node(int n) {
        left = null;
        right = null;
        data = n;
    }

    /**
     * takes the
     * @param n and sets it as left node
     */
    public void setLeft(Node n) {
        left = n;
    }

    /**
     * takes the
     * @param n and sets it as right node
     */
    public void setRight(Node n) {
        right = n;
    }

    /**
     * @return left node
     */
    public Node getLeft() {
        return left;
    }

    /**
     * @return right node
     */
    public Node getRight() {
        return right;
    }

    /**
     * Sets data
     * @param d to node
     */
    public void setData(int d) {
        data = d;
    }

    /**
     * @return data from node
     */
    public int getData() {
        return data;
    }
}

