import java.util.Scanner;

public class TestTree {

        public static void main(String[] args)
        {
            Scanner scan = new Scanner(System.in);
            BinaryTree bT = new BinaryTree();
            char ch;

            do
            {
                System.out.print("\n" +"--1. Add a node "+"\n"
                        +"--2. Delete a node" +"\n"
                        +"--3. Show min node" +"\n"
                        +"--4. Show max node" +"\n"
                        +"--5. Find a node" +"\n"
                        +"--6. Print all nodes" +"\n"
                        +"--7. Exit" +"\n"
                        +"--Enter your choice: ");


                int choice = scan.nextInt();
                switch (choice)
                {
                    case 1 :
                        System.out.print("Enter integer : ");
                        bT.insert( scan.nextInt() );
                        break;
                    case 2 :
                        System.out.print("Enter integer : ");
                        bT.delete( scan.nextInt() );
                        break;
                    case 3 :
                        System.out.println("Min node: "+ bT.findMin());
                        break;
                    case 4 :
                        System.out.println("Max node: "+ bT.findMax());
                        break;
                    case 5 :
                        System.out.print("Enter integer to search: ");
                        System.out.print("Result : "+ bT.search( scan.nextInt() ));
                        break;
                    case 6 :
                        System.out.print("All Nodes: "+" "+ bT.countNodes()+ "");
                        System.out.println("\n"+"--Post-order : ");
                        bT.postorder();
                        System.out.println("\n"+"Pre-order : ");
                        bT.preorder();
                        System.out.println("\n"+"In order : ");
                        bT.inorder();
                        break;
                    case 7 :
                        System.exit(0);
                        break;
                    default :
                        System.out.println("Error \n ");
                        break;
                }

                System.out.print("Do you want to continue? (Type Y or N): ");
                ch = scan.next().charAt(0);
            } while (ch == 'Y'|| ch == 'y');
        }
    }

