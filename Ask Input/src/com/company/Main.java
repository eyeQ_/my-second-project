
package com.company;

import com.company.Services.MSWordTextFinder;
import com.company.Services.PDFTextFinder;
import com.company.Services.TXTTExtFinder;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

//ask's the user for a file path and for a word to search
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter a file path: ");
        String n = reader.next();
        System.out.println("Enter a Word to search: ");
        String z = reader.next();

//if the file is a pdf
        if (n.endsWith("pdf")) {

            PDFTextFinder q = new PDFTextFinder();
            q.findTextInPDFDocument(n, z);
            // System.out.println(q.findTextInPDFDocument(n, z));
        }
//if the file is a txt
        else if (n.endsWith("txt")) {
                TXTTExtFinder v = new TXTTExtFinder();
                v.findTextInTxtDocument(n, z);
                // System.out.println(v.findTextInTxtDocument(n, z));
            }
//if the file is a doc
          else{
                MSWordTextFinder k = new MSWordTextFinder();
                k.findTextInWordDocument(n, z);
                // System.out.println(k.findTextInWordDocument(n, z));

            }
        }

    }

/**
 * My file path inputs:

 * C:\Users\georg\Desktop\Training\File.txt
 * C:\Users\georg\Desktop\Training\Excersise1.pdf
 * C:\Users\georg\Desktop\Training\TestWordDoc.doc
 */

