package com.company.Services;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MSWordTextFinder {

    //take as input the file path and a word to search in that file
    public void findTextInWordDocument(String path, String word) throws IOException {


        File fin = new File(path);

        FileInputStream fis = new FileInputStream(fin);
        HWPFDocument doc = new HWPFDocument(fis);

        Range range = doc.getRange();
        for (int i = 0; i < range.numParagraphs(); i++) {
            Paragraph par = range.getParagraph(i);
            String g = par.text();

            if (g.contains(word)) {
                range.getParagraph(i);
                System.out.println("paragraph " + (i + 1));
                String x = "word:" + " " + word + ", " + "Found" + ": " + g.contains(word);
                System.out.println(x);
            }
        }

    }
}