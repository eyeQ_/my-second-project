
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;

public class PDFTextFinder {

    public String findTextInPDFDocument(String path, String word) throws IOException {

      //  word = "java";
      //  path = "C:\\Users\\georg\\Desktop\\Dum\\Excersise1.pdf";
        File myFile = new File(path);

        try (PDDocument doc = PDDocument.load(myFile)) {

            PDFTextStripper stripper = new PDFTextStripper();
            String text = stripper.getText(doc);
            System.out.println(text);
            String x = ("word:" +word + ", " +"Found"+ ": " + text.contains(word));
            System.out.println(x);
            return x;

        }

    }
}