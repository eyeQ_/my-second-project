

import org.apache.poi.hwpf.HWPFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MSWordTextFinder {

    public String findTextInWordDocument(String path, String word) throws IOException{

       File fin = new File(path);

            HWPFDocument doc;
            try (FileInputStream fis = new FileInputStream(fin)) {
                doc = new HWPFDocument(fis);
            }
            String text = doc.getDocumentText();
            System.out.println(text);
            String s = "word:" + word + ", " + "Found" + ": " + text.contains(word);
            System.out.println(s);
        return s;


    }

}



